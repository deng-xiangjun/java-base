/**
 * @author xiangjun.deng
 * email 17670934918dxj@sina.com
 * @date 2021-10-18 20:17
 * @desc 测试逻辑运算符的短路现象
 */
public class Test02 {
    public static void main(String[] args) {
        boolean b = test01("11") && test01("22");
        System.out.println(b);
        int i1=0177;
        char c = 0xffff;
        System.out.println(Integer.toBinaryString(c));
        byte d= 0x7f;
        System.out.println(d);
        System.out.println(i1);
    }
    public static boolean test01(String msg)
    {
        System.out.println(msg);
        return false;
    }


}
