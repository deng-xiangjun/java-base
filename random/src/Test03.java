/**
 * @author xiangjun.deng
 * email 17670934918dxj@sina.com
 * @date 2021-10-18 20:35
 * @desc
 */
public class Test03 {
    public static void main(String[] args) {
        int i1 = 10;
        int i2 = 5;
        System.out.println(i1 & i2);
        System.out.println(i1 ^ i2);
        System.out.println(i1 | i2);
        int i3 = (int) 29.7;
        System.out.println(i3);
        System.out.println(Integer.toBinaryString(~10));
    }
}
