package com;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * @author xiangjun.deng
 * email 17670934918dxj@sina.com
 * @date 2021/8/17 9:51
 * @desc
 */
public class Demo01 {
    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        if (calendar instanceof GregorianCalendar)
            System.out.println("It is an instance of GregorianCalendar");
        int day=calendar.get(Calendar.DAY_OF_MONTH);
        System.out.println(day);
        int i = calendar.get(Calendar.DATE);
        System.out.println(i);


    }

}
