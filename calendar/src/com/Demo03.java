package com;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author xiangjun.deng
 * email 17670934918dxj@sina.com
 * @date 2021/8/17 10:13
 * @desc
 */
public class Demo03 {
    public static void main(String[] args) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatYYYYMMDD = new SimpleDateFormat("yyyyMMdd");
        String startTime="";
        String endTime="";
        Calendar c  = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, - 1);
        Date d = c.getTime();
        endTime = format.format(d);
        System.out.println(endTime);
        c.add(Calendar.MONTH, -1);
        c.add(Calendar.DATE, 1);
        Date m = c.getTime();
        startTime = format.format(m);
        System.out.println(startTime);
    }
}
