package com.jdk;

import com.service.UserService;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author xiangjun.deng
 * email 17670934918dxj@sina.com
 * @date 2021/8/9 20:53
 * @desc
 */
public class Proxy01 {

  public static UserService getObj(UserService userService)
  {
      //获取被代理对象的类加载器
      ClassLoader classLoader = userService.getClass().getClassLoader();
      //获取被代理对象实现的接口
      Class[] classes = userService.getClass().getInterfaces();
      //对原始方法执行进行拦截并增强
      InvocationHandler ih = new InvocationHandler() {
          @Override
          public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
              System.out.println("1111");
              //前置增强内容
              Object ret = method.invoke(userService, args);
              //后置增强内容
              System.out.println("刮大白2");
              return ret;
          }
      };
      //使用原始被代理对象创建新的代理对象
      UserService proxy = (UserService) Proxy.newProxyInstance(classLoader,classes,ih);
      return proxy;
  }

}

