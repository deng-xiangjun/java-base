package com.test;

import com.jdk.Proxy01;
import com.service.UserService;
import com.service.impl.UserServiceImpl;

/**
 * @author xiangjun.deng
 * email 17670934918dxj@sina.com
 * @date 2021/8/9 20:56
 * @desc
 */
public class Test01 {
    public static void main(String[] args) throws InterruptedException {

            UserService userService = new UserServiceImpl();
            UserService proxyService = Proxy01.getObj(userService);
            synchronized (proxyService)
            {
               proxyService.hashCode();
                proxyService.wait(3);
            }


    }
}
