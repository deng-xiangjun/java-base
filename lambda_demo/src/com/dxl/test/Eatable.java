package com.dxl.test;

/**
 * @author xiangjun.deng
 * email 17670934918dxj@sina.com
 * @date 2021-12-7 14:34
 * @desc
 */
public interface Eatable {
    void eat();
}
