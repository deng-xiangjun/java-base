package com.dxl.test;

/**
 * @author xiangjun.deng
 * email 17670934918dxj@sina.com
 * @date 2021-12-7 14:33
 * @desc
 */
public class LambdaDemo01 {
    public static void main(String[] args) {
        //在主方法中调用useEatable方法
        Eatable e = new EatableImpl();
        useEatable(e);

        //匿名内部类
        useEatable(new Eatable() {
            @Override
            public void eat() {
                System.out.println("一天一苹果，医生远离我");
            }
        });

        //Lambda表达式 组成Lambda表达式的三要素：
        //
        //- 形式参数，箭头，代码块
        useEatable(() -> {
            System.out.println("一天一苹果，医生远离我，lambda表达式");
        });
    }
    private static void useEatable(Eatable e) {
        e.eat();
    }

}

