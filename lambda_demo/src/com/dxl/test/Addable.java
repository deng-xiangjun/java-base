package com.dxl.test;

/**
 * @author xiangjun.deng
 * email 17670934918dxj@sina.com
 * @date 2021-12-7 14:38
 * @desc
 */
public interface Addable {
    int add(int x,int y);
}
