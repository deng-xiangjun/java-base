package com.preferences;

import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021-12-29 10:13
 * @desc desc
 * ========================================================
 */
public class PreferencesDemo {
    public static void main(String[] args) throws BackingStoreException {
        Preferences prefs = Preferences.userNodeForPackage(PreferencesDemo.class);
        prefs.put("Location","SZ");
        prefs.put("name","john");
        prefs.putBoolean("Are there witches",true);
        prefs.putInt("age",18);
        int count = prefs.getInt("count", 0);
        count++;
        System.out.println(count);
        prefs.putInt("count",count);
        String[] keys = prefs.keys();
        for (String key : keys) {
            System.out.println(key +": "+prefs.get(key,null));
        }
        System.out.println(prefs.getInt("age",0));
    }
}
