package com.file;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.zip.*;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/26 15:49
 * @desc desc
 * ========================================================
 */
public class ZipCompress {
    public static void main(String[] args) throws IOException {
        String [] filesArr = new String[] {"WMS系统表.txt","data.txt","完美主义.mp3"};
        FileOutputStream outputStream = new FileOutputStream("test.zip");
        CheckedOutputStream cos = new CheckedOutputStream(outputStream,new Adler32());
        ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(cos));
        zos.setComment("a test of java Zipping");
        for (String fileName : filesArr) {
            zos.putNextEntry(new ZipEntry(fileName));
            InputStream in = new FileInputStream(fileName);
          BufferedInputStream bis = new BufferedInputStream(new FileInputStream(fileName));
          byte[] buffer = new byte[1024*5];
          int read;
            while((read=bis.read(buffer))!=-1) {
               zos.write(buffer,0,read);
            }
            zos.flush();
            bis.close();
        }
        zos.close();
        System.out.println(" close 之后 checksum: "+cos.getChecksum().getValue());
//        FileInputStream inputStream = new FileInputStream("test.zip");
//        CheckedInputStream cis = new CheckedInputStream(inputStream, new Adler32());
//        ZipInputStream zis = new ZipInputStream(new BufferedInputStream(cis));
//        ZipEntry ze;
//        while ((ze=zis.getNextEntry())!=null) {
//            System.out.println("Reading file: "+ze);
//            System.out.println(ze.getComment());
//            int x;
//            while ((x=zis.read())!=-1) {
//                //System.out.write(x);
//            }
//        }
//        System.out.println();
//        System.out.println(" close 之前 checksum: "+cis.getChecksum().getValue());
//        zis.close();
//        System.out.println(" close 之后 checksum: "+cis.getChecksum().getValue());
        File file;
        ZipFile zipFile = new ZipFile("test.zip");
        Enumeration<? extends ZipEntry> e = zipFile.entries();
        while (e.hasMoreElements()) {
            ZipEntry ze = e.nextElement();
            System.out.println("Reading file: "+ze);
        }
    }
}
