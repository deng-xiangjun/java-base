package com.file;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TestDate {
    public static void main(String[] args) {
        //SimpleDateFormat的setLenient(true)
        //这种情况下java会把你输入的日期进行计算，比如55个月那么就是4年以后，这时候年份就会变成03年了
        String dob= "1/55/1999";
        Date dateofbirth =null;
              SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                try {
                  dateFormat.setLenient(true);
                  dateofbirth = dateFormat.parse(dob);
                } catch (Exception e) {
                  e.printStackTrace();
                }
        System.out.println(dateofbirth);
        //输出是Tue Jul 01 00:00:00 CST 2003

        //SimpleDateFormat的setLenient(false)
        //这种情况下java不会把你输入的日期进行计算，比如55个月那么就是不合法的日期了，直接异常
        String dob2= "1/55/1999";
        Date dateofbirth2 =null;
              SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd/MM/yyyy");
                try {
                  dateFormat2.setLenient(false);
                  dateofbirth2 = dateFormat2.parse(dob2);
                } catch (Exception e) {
                  e.printStackTrace();
                }
        System.out.println(dateofbirth2);
        /*
         * 输出如下
         * java.text.ParseException: Unparseable date: "1/55/1999"
            at java.text.DateFormat.parse(Unknown Source)
            at com.test.date.TestDate.main(TestDate.java:28)
            null
         */

    }
}