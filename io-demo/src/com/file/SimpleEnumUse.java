package com.file;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/25 16:41
 * @desc desc
 * ========================================================
 */
public class SimpleEnumUse {
    public static void main(String[] args) {
        Spiciness hot = Spiciness.HOT;
        System.out.println(hot);
        Spiciness[] values = Spiciness.values();
        for (int i = 0; i < values.length; i++) {
            System.out.println(values[i]+", ordinal "+values[i].ordinal());
        }
    }
}
