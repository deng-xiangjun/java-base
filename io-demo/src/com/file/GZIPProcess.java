package com.file;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/26 14:43
 * @desc desc
 * ========================================================
 */
public class GZIPProcess {
    public static void main(String[] args) throws IOException {
        File file = new File("WMS系统表.txt");
        BufferedReader br = new BufferedReader(new FileReader(file));
        BufferedOutputStream bos = new BufferedOutputStream(new GZIPOutputStream(new FileOutputStream("wms系统表.gz")));
        String c;
        while ((c=br.readLine())!=null) {
            bos.write(c.getBytes(StandardCharsets.UTF_8));
            bos.write("\n".getBytes(StandardCharsets.UTF_8));
        }
        bos.close();
        br.close();

        System.out.println("Reading file");
        BufferedReader in = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream("wms系统表.gz"))));
        String s;
        while ((s=in.readLine())!=null) {
            System.out.println(s);
        }
    }
}
