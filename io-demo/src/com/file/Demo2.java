package com.file;

import java.io.File;
import java.io.IOException;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/10 23:29
 * @desc ========================================================
 */
public class Demo2 {
    public static void main(String[] args) throws IOException {
        File file = new File("io-demo//aaa//a.txt");
        File parentFile = file.getParentFile();
        System.out.println(parentFile);
        if(parentFile !=null) {
            boolean flag =parentFile.mkdirs();
            System.out.println(flag);
        }
        //必须保证文件所在的文件夹存在
        file.createNewFile();
    }
}
