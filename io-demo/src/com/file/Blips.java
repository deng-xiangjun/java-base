package com.file;

import java.io.*;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/26 21:27
 * @desc desc
 * ========================================================
 */
public class Blips {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        System.out.println("Constructor objects:");
        Blips1 b1 = new Blips1();
        Blips2 b2 = new Blips2();
        OutputStream out = new FileOutputStream("Blips.out");
        ObjectOutputStream oos = new ObjectOutputStream(out);
        System.out.println("saving object");
        oos.writeObject(b1);
        oos.writeObject(b2);
        oos.close();
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("Blips.out"));
        System.out.println("Recovering b1: ");
        b1 = (Blips1) ois.readObject();
        System.out.println("Recovering b2: ");
        b2 = ((Blips2) ois.readObject());

    }
}

class Blips1 implements Externalizable {

    public Blips1() {
        System.out.println("Blips1 Constructor");
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        System.out.println("Blips1 writeExternal");
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        System.out.println("Blips1 readExternal");
    }
}

class Blips2 implements Externalizable {

    public Blips2() {
        System.out.println("Blips2 Constructor");

    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        System.out.println("Blips2 writeExternal");
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        System.out.println("Blips2 readExternal");
    }
}
