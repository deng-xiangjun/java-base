package com.file;

import java.io.*;
import java.nio.IntBuffer;
import java.nio.channels.FileChannel;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/25 20:41
 * @desc desc
 * ========================================================
 */
public class MappedIO {
    private static int numOfInts = 40000000;
    private static int numOfBuffInts = 200000;

    private static abstract class Tester {
        private String name;

        public Tester(String name) {
            this.name = name;
        }

        public void runTest() throws IOException {
            System.out.print(name + ": ");
            long start = System.nanoTime();
            test();
            long end = System.nanoTime();
            double duration = end - start;
            System.out.format("%.2f\n", duration / 1e9);
        }

        public abstract void test() throws IOException;
    }

    private static Tester[] tests = new Tester[]{
            new Tester("Stream write") {
                @Override
                public void test() throws IOException {
                    DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("temp.tmp")));
                    for (int i = 0; i < numOfInts; i++) {
                        out.writeInt(i);
                    }
                    out.close();
                }
            },
            new Tester("Mapped Write") {
                @Override
                public void test() throws IOException {
                    File file = new File("temp.tmp");
                    FileChannel fc = new RandomAccessFile(file, "rw").getChannel();
                    IntBuffer intBuffer = fc.map(FileChannel.MapMode.READ_WRITE, 0, fc.size()).asIntBuffer();
                    for (int i = 0; i < numOfInts; i++) {
                        intBuffer.put(i);
                    }
                    fc.close();
                }
            },
            new Tester("Stream Read") {
                @Override
                public void test() throws IOException {
                    File file = new File("temp.tmp");
                    DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(file)));
                    for (int i = 0; i < numOfInts; i++) {
                        dis.readInt();
                    }
                    dis.close();
                }
            },
            new Tester("Mapped Read") {
                @Override
                public void test() throws IOException {
                    File file = new File("temp.tmp");
                    FileChannel fc = new RandomAccessFile(file, "rw").getChannel();
                    IntBuffer ib = fc.map(FileChannel.MapMode.READ_WRITE, 0, fc.size()).asIntBuffer();
                    while (ib.hasRemaining()) {
                        ib.get();
                    }
                    fc.close();
                }
            },
            new Tester("Stream Read/write") {
                @Override
                public void test() throws IOException {
                    File file = new File("temp.tmp");
                    RandomAccessFile rm= new RandomAccessFile(file, "rw");
                    rm.writeInt(1);
                    for (int i = 0; i < numOfBuffInts; i++) {
                        rm.seek(rm.length()-4);
                        rm.writeInt(rm.readInt());
                    }
                    rm.close();
                }
            },
            new Tester("Mapped Read/Write") {
                @Override
                public void test() throws IOException {
                    File file = new File("temp.tmp");
                    RandomAccessFile rm= new RandomAccessFile(file, "rw");
                    FileChannel fc = rm.getChannel();
                    IntBuffer ib = fc.map(FileChannel.MapMode.READ_WRITE, 0, fc.size()).asIntBuffer();
                    ib.put(1);
                    for (int i = 0; i < numOfBuffInts; i++) {
                        ib.put(ib.get(i));
                    }
                    fc.close();

                }
            }
    };

    public static void main(String[] args) throws IOException {
        for (Tester test : tests) {
            test.runTest();
        }
        //output
        // Stream write: 2.28
        //Mapped Write: 0.06
        //Stream Read: 2.56
        //Mapped Read: 0.01
        //Stream Read/write: 3.23
        //Mapped Read/Write: 0.00
    }

}
