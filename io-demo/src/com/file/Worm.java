package com.file;



import java.io.*;
import java.util.Random;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/26 18:28
 * @desc desc
 * ========================================================
 */
public class Worm implements Serializable {
    private static Random rand = new Random(47);
    private Data[] d = new Data[] {
            new Data(rand.nextInt(10)),
            new Data(rand.nextInt(10)),
            new Data(rand.nextInt(10))
    };
    private Worm next;
    private char c;
    public Worm(int i,char x) {
        System.out.println("Worm constructor: "+i);
        c=x;
        if(--i>0) {
            next = new Worm(i,(char) (x+1));
        }
    }

    @Override
    public String toString() {
       StringBuilder sb = new StringBuilder(" :");
       sb.append(c);
       sb.append("(");
        for (Data data : d) {
            sb.append(data);
        }
        sb.append(")");
        if(next!=null) {
            sb.append(next);
        }
        return sb.toString();
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Worm w = new Worm(6,'a');
        System.out.println("w = "+w);
        OutputStream out = new FileOutputStream("worm.out");
        ObjectOutputStream os = new ObjectOutputStream(out);
        os.writeObject("Worm storage\n");
        os.writeObject(w);
        out.close();
        InputStream in = new FileInputStream("worm.out");
        ObjectInputStream ois = new java.io.ObjectInputStream(in);
        String  s = ((String) ois.readObject());
        Worm worm = (Worm) ois.readObject();
        System.out.println(s + "w2= "+worm);
        ois.close();
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bout);
        oos.writeObject("Worm storage\n");
        oos.writeObject(w);
        oos.close();
        ObjectInputStream ins = new ObjectInputStream(new ByteArrayInputStream(bout.toByteArray()));
        String s1 = (String) ins.readObject();
        Worm w3 = (Worm) ins.readObject();
        System.out.println(s1 +"w3= "+w3);


    }
}
class Data implements Serializable {
    private int n;
    public Data(int n) {
        this.n = n;
    }
    @Override
    public String toString() {
       return Integer.toString(n);
    }
}

