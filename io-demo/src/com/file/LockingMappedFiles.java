package com.file;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/26 12:49
 * @desc desc
 * ========================================================
 */
public class LockingMappedFiles {
    private static int length = 0x8ffffff;
    static  FileChannel fc;
    public static void main(String[] args) throws IOException {
        File file = new File("test.dat");
        fc = new RandomAccessFile(file, "rw").getChannel();
        MappedByteBuffer out = fc.map(FileChannel.MapMode.READ_WRITE, 0, length);
        for (int i = 0; i < length; i++) {
            byte b = out.get();
            System.out.println((char)b);
        }
       // new LockAndModify(out,0,length/3);
        //new LockAndModify(out,length/2,length/2+length/4);
;    }
    private static class LockAndModify extends Thread {
        private int start,end;
        private ByteBuffer buff;
        public LockAndModify (ByteBuffer mob,int start,int end) {
            this.start=start;
            this.end=end;
            mob.limit(end);
            mob.position(start);
            this.buff=mob.slice();
            start();
        }
        @Override
        public void run() {
            try {
                FileLock lock = fc.lock(start,end,false);
                System.out.println("Locked: "+start+"to: "+end);
                while (buff.position()<buff.limit()-1) {
                    buff.put((byte) (buff.get()+1));
                }
                lock.release();
                System.out.println("Released: "+start+"to: "+end);
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
    }
}
