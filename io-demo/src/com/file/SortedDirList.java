package com.file;

import java.io.File;
import java.io.FilenameFilter;
import java.util.regex.Pattern;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/11 14:16
 * @desc ========================================================
 */
public class SortedDirList {
    public static void main(String[] args) {
        DirList dl = new DirList();
        String[] fullList = dl.list();
        for (String s : fullList) {
            System.out.println(s);
        }
        System.out.println("====================================");
        String[] subList = dl.list(".+\\.mp3");
        long total = 0;
        long fs=0;
        for (String s : subList) {
            fs = new File(s).length();
            System.out.println(s+", "+fs+"bytes");
            total+=fs;
        }
        System.out.println("===================================");
        System.out.println(subList.length + " file(s), " + total + " bytes");
    }
}

class DirList {
    File path;
    public DirList() {
        path = new File(".");
    }
    public DirList(File path) {
      this. path =  path;
    }
    public String[] list() {
        return path.list();
    }
    public String[] list(String regex) {
      return  path.list(new FilenameFilter() {
            private Pattern pattern = Pattern.compile(regex);
            @Override
            public boolean accept(File dir, String name) {
                return pattern.matcher(name).matches();
            }
        });
    }
}
