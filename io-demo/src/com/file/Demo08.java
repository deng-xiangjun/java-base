package com.file;

import com.file.util.DirUtil;

import java.io.File;
import java.io.IOException;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/11 15:55
 * @desc ========================================================
 */
public class Demo08 {
    public static void main(String[] args) throws IOException {
        String s=".dxj";
        String a = "aaaaa.dxj";
        System.out.println(a.endsWith(s));

        String b="b";
        System.out.println("."+s);
        System.out.println("====================");
        File file = new File(".");
        for (File listFile : file.listFiles()) {
            System.out.println("CanonicalFile: "+listFile.getCanonicalFile());
            System.out.println("CanonicalPath: "+listFile.getCanonicalPath());
        }
        System.out.println("======================");
        //DirUtil.TreeInfo walk = DirUtil.walk(file, ".+\\.*");
        DirUtil.TreeInfo walk = DirUtil.walk(file);
        DirUtil.print(walk.dirs);
        DirUtil.print(walk.files);
    }
}
