package com.file;

import com.file.util.ProcessFiles;

import java.io.File;
import java.io.IOException;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/11 16:57
 * @desc desc
 * ========================================================
 */
public class Demo10 {
    public static void main(String[] args) throws IOException {
        ProcessFiles ps = new ProcessFiles(new ProcessFiles.Strategy() {
            @Override
            public void process(File file) {
                System.out.println(file);
            }
        },"java");
        ps.start(args);
    }
}
