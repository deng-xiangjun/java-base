package com.file;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/25 16:53
 * @desc desc
 * ========================================================
 */
public class LargeMappedFiles {
    private static final int LENGTH = 0X8FFFFFF;

    public static void main(String[] args) throws IOException {
        MappedByteBuffer mappedByteBuffer = new RandomAccessFile("testMapper.txt", "rw").getChannel().map(FileChannel.MapMode.READ_WRITE, 0, LENGTH);
        for (int i = 0; i < LENGTH; i++) {
            mappedByteBuffer.put((byte)'x');
        }
        System.out.println("Finished writing");
        for (int i = LENGTH/2;i<LENGTH/2+6;i++) {
            byte b = mappedByteBuffer.get(i);
            System.out.println((char)b);
        }
    }
}
