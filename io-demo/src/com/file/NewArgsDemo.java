package com.file;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/25 9:58
 * @desc desc
 * ========================================================
 */
public class NewArgsDemo {
    public static void main(String[] args) {
        printArray(new Integer[] {1,3,2,4});
    }

    static void printArray(Object ... args) {
        for (Object arg : args) {
            System.out.print(arg+" ");
        }
        System.out.println();
    }
}
