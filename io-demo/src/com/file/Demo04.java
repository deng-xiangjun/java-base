package com.file;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/11 0:00
 * @desc ========================================================
 */
public class Demo04 {
    public static void main(String[] args) {
        File file = new File("D:\\java-resouce\\javaee进阶\\day12_IO流01");
        Map<String,Integer> map = new HashMap<>();
         getCount(file,map);
        System.out.println(map);

    }

    private static Map<String, Integer> getCount(File file, Map<String,Integer> map) {
        File[] files = file.listFiles();
        for (File f : files) {
            if(f.isFile()) {
                String fileName = f.getName();
                String[] split = fileName.split("\\.");
                String suffix = split[1];
                if("xlsx".equals(suffix)) {
                    System.out.println(fileName);
                }
                if(map.containsKey(suffix)) {
                    Integer count = map.get(suffix);
                    count++;
                    map.put(suffix,count);
                }else {
                    map.put(suffix,1);
                }
            }else {
                getCount(f,map);
            }

        }
        return map;
    }
}
