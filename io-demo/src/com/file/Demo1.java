package com.file;

import javax.xml.bind.Element;
import java.io.File;
import java.io.IOException;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/10 23:07
 * @desc ========================================================
 */
public class Demo1 {
    public static void main(String[] args) throws IOException {
        //相对于当前模块
       // File file1 = new File("/a.txt");
        File file1 = new File(".");
        System.out.println(file1.getAbsolutePath());
        System.out.println(file1.getParentFile());
        System.out.println(file1.exists());
        String[] list = file1.list();
        for (String s : list) {
            System.out.println(s);
        }
        //file1.createNewFile();
    }

}
