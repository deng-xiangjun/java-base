package com.file;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import java.io.*;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/11 1:00
 * @desc ========================================================
 */
public class Demo06 {
    private final static String  ORIGIN_URL="D:\\music\\music\\music\\jay\\周杰伦-专辑《周杰伦同名专辑》\\完美主义.mp3";
    public static void main(String[] args) {
        FileOutputStream fos=null;
        FileInputStream fis=null;
        try {
            fis = new FileInputStream(ORIGIN_URL);

            int index = ORIGIN_URL.lastIndexOf("\\");
            String fileName = ORIGIN_URL.substring(index+1, ORIGIN_URL.length());
            fos = new FileOutputStream(fileName);
            long start = System.currentTimeMillis();
            byte[] buffer = new byte[1024];
            int b=0;
            while ((b=fis.read(buffer,0,buffer.length))!=-1){
                fos.write(buffer,0,b);
            }
            long end = System.currentTimeMillis();
            System.out.println(end-start);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException exception){
            exception.printStackTrace();
        }
            finally {
            if(fos!=null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(fis!=null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
//        int index = ORIGIN_URL.lastIndexOf("\\");
//        String substring = ORIGIN_URL.substring(index+1, ORIGIN_URL.length());
//        System.out.println(substring);
    }
}
