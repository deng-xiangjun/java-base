package com.file;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/11 12:26
 * @desc ========================================================
 */
public class Demo07 {
    public static void main(String[] args) {
          File file = new File("src/com/file");
        String[] list;
        if(args.length==0) {
              list = file.list();
            for (String s : list) {
                System.out.println(new File(s).getName());
            }
          }else {
               list = file.list(new DirFilter(".+\\.java$"));
          }

        for (String s : list) {
            System.out.println(s);
        }
    }
    public static void main1(String args[]) {
        String str = "11.java";
        //str.matches("java")
        String pattern = ".+\\.java$";
        System.out.println(pattern);
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(str);
        System.out.println(m.matches());
    }
}

class DirFilter implements FilenameFilter {
    private Pattern pattern;
    public DirFilter (String regex) {
        this.pattern = Pattern.compile(regex);
    }

    @Override
    public boolean accept(File dir, String name) {
       return pattern.matcher(name).matches();
    }






}
