package com.file;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.List;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/27 23:19
 * @desc desc
 * ========================================================
 */
public class RecoverCADState {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("CADState.out"));
        Circle.deSerializeStaticState(ois);
        Square.deSerializeStaticState(ois);
       Line.deSerializeStaticState(ois);
        List<Shape> shapes = (List<Shape>) ois.readObject();
        ois.close();
        System.out.println(shapes);
    }
}
