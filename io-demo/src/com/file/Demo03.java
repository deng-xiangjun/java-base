package com.file;

import java.io.File;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/10 23:40
 * @desc  删除一个有内容的文件夹
 * ========================================================
 */
public class Demo03 {
    public static void main(String[] args) {
        File file = new File("C:\\Users\\Administrator\\Desktop\\aaaa");
        removeDirectory(file);

    }

    public static void removeDirectory(File file) {
        if(file.isFile()) {
            file.delete();
        } else {
            File[] filesArr = file.listFiles();
            for (File f : filesArr) {
                if(f.isFile()) {
                    f.delete();
                }else {
                    removeDirectory(f);
                }
                f.delete();
            }
        }
        file.delete();
    }
}
