package com.file;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/11 0:42
 * @desc ========================================================
 */
public class Demo05 {
    public static void main(String[] args) {
        File file = new File("a.txt");
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file,true);
            fos.write(99);
            fos.write(100);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
