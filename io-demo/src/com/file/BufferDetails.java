package com.file;

import java.nio.ByteBuffer;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/23 6:52
 * @desc desc
 * ========================================================
 */
public class BufferDetails {
    private static final Integer B_SIZE=20;
    public static void main(String[] args) {
        ByteBuffer buff = ByteBuffer.allocate(B_SIZE);
        buff.asCharBuffer().put("abcde");
        System.out.println("capacity: "+buff.capacity());
        System.out.println("limit: "+buff.limit());
        System.out.println("mark: "+buff.mark());
        System.out.println("position: "+buff.position());
        System.out.println("remaining: "+buff.remaining());
        while (buff.hasRemaining()) {
            System.out.println(buff.getChar());
        }
        System.out.println("=======================");
        System.out.println("capacity: "+buff.capacity());
        System.out.println("limit: "+buff.limit());
        System.out.println("mark: "+buff.mark());
        System.out.println("position: "+buff.position());
        System.out.println("remaining: "+buff.remaining());
    }
}
