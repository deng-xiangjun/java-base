package com.file;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/25 16:40
 * @desc desc
 * ========================================================
 */
public enum Spiciness {
    NOT,MILD,MEDIUM,HOT,FLAMING
}
