package com.file;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/22 22:43
 * @desc desc
 * ========================================================
 */
public class IntBufferDemo {
    private static final Integer B_SIZE = 1024;

    public static void main(String[] args) {
        ByteBuffer buff = ByteBuffer.allocate(B_SIZE);
        IntBuffer intBuffer = buff.asIntBuffer();
        intBuffer.put(new int[]{1,2,3,4,5,6});
        System.out.println(intBuffer.get(3));
        intBuffer.put(3,122222);
        System.out.println(intBuffer.get(3));
        intBuffer.flip();
        while (intBuffer.hasRemaining()) {
            System.out.println(intBuffer.get());
        }
    }
}
