package com.file;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/13 23:34
 * @desc desc
 * ========================================================
 */
public class RandomAccessFileDemo {
    private static String file = "rTest.txt";

    public static void main(String[] args) throws IOException {
        RandomAccessFile rf = new RandomAccessFile(file,"rw");
        for (int i = 0; i < 7; i++) {
            rf.writeDouble(i*1.414);
        }
        rf.writeUTF("The end of file");
        rf.close();
        display();
        RandomAccessFile rf1 = new RandomAccessFile(file,"rw");
        rf1.seek(5*8);
        System.out.println(rf1.getFilePointer());
        rf1.writeDouble(47.001);
        rf.close();
        display();
    }

    private static void display() throws IOException {
        RandomAccessFile rf = new RandomAccessFile(file,"r");
        for (int i = 0; i < 7; i++) {
            System.out.println("value "+i+": "+rf.readDouble());
        }
        System.out.println(rf.readUTF());

        rf.close();
    }

}
