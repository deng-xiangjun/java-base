package com.file;

import com.file.util.ProcessFiles;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/11 16:58
 * @desc desc
 * ========================================================
 */
public class Demo11 {
    public static void main(String[] args) throws ParseException, IOException {
        File file = new File("src/com/file/Demo2.java");
        System.out.println("can read:"+file.canRead());
        System.out.println("can write:"+file.canWrite());
        System.out.println("absolute path:"+file.getAbsolutePath());
        System.out.println("get name:"+file.getName());
        System.out.println("get parent:"+file.getParent());
        System.out.println("get path:"+file.getPath());
        System.out.println("get length:"+file.length());
        System.out.println("lastModified:"+file.lastModified());

       SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        long time = sdf.parse("11/12/2021").getTime();
        ProcessFiles ps = new ProcessFiles(new ProcessFiles.Strategy() {
            @Override
            public void process(File file) {
                if(file.lastModified() < time) {
                    System.out.println(file);
                }
            }
        },"java");
        ps.start(args);
    }
}
