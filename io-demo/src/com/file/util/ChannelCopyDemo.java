package com.file.util;

import java.io.*;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/14 23:22
 * @desc desc
 * ========================================================
 */
public class ChannelCopyDemo {

    private final static Integer B_SIZE = 1024;

    public static void main1(String[] args) throws IOException {
        FileChannel fc = new FileInputStream("完美主义.mp3").getChannel();
        ByteBuffer buffer = ByteBuffer.allocate(B_SIZE);
        FileChannel channel = new FileOutputStream("C:\\Users\\Administrator\\Desktop\\a.map3").getChannel();
        int res;
        while (fc.read(buffer) != -1) {
            //让缓冲器中的数据准备好被其它人读写
            buffer.flip();
            channel.write(buffer);
            //重置缓冲区准备下次读取数据
            buffer.clear();
//        }

        }
        fc.close();
        channel.close();
    }

    public static void main(String[] args) throws IOException {
        FileChannel fc = new FileInputStream("完美主义.mp3").getChannel();
        ByteBuffer buffer = ByteBuffer.allocate(B_SIZE);
        FileChannel channel = new FileOutputStream("C:\\Users\\Administrator\\Desktop\\完美主义.map3").getChannel();
       fc.transferTo(0,fc.size(),channel);
    }
}