package com.file.util;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/11 16:38
 * @desc ========================================================
 */
public class ProcessFiles {
    public interface Strategy {
        void process(File file);
    }

    private String ext;
    private Strategy strategy;

    public ProcessFiles(Strategy strategy, String ext) {
        this.strategy = strategy;
        this.ext = ext;
    }

    public void start(String[] args) throws IOException {
        if (args.length == 0) {
            processTreeDirectory(new File("."));
        } else {
            for (String arg : args) {
                File file = new File(arg);
                if (file.isDirectory()) {
                    processTreeDirectory(file);
                } else {
                    if (!arg.endsWith("." + ext)) {
                        arg += "." + ext;
                    }
                    strategy.process(new File(arg));
                }
            }
        }
    }

    public void processTreeDirectory(File root) throws IOException {
        for (File file : DirUtil.walk(root, ".*\\." + ext).files) {
            strategy.process(file.getCanonicalFile());
        }
    }
}
