package com.file.util;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/14 22:39
 * @desc desc
 * ========================================================
 */
public class Demo12 {
    public static void main(String[] args) throws IOException {
        FileChannel channel = new FileOutputStream("data.txt").getChannel();
        ByteBuffer byteBuffer = ByteBuffer.wrap("不要忘记梦想".getBytes(StandardCharsets.UTF_8));
        channel.write(byteBuffer);
        channel.close();
        //给文件末尾添加一句话
        File file = new File("data.txt");
        channel = new RandomAccessFile(file, "rw").getChannel();
        //设置此通道文件的位置
        //size()返回文件的大小
        channel.position(channel.size());
        channel.write(ByteBuffer.wrap("看看我是不是添加到了最后".getBytes(StandardCharsets.UTF_8)));
        channel.close();

        channel = new FileInputStream(file).getChannel();
        //
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        channel.read(buffer);
        //一旦使用ByteBuffer存储数据，必须使用flip()方法。让缓冲器做好被别人读取数据的准备
        buffer.flip();
        byte[] bytes = new byte[1024];
        int i =0;
        while (buffer.hasRemaining()) {
            byte b = buffer.get();
            bytes[i]=b;
            i++;
        }
        //已经UTF-8的形式解码
        System.out.println(new String(bytes,"UTF-8"));
        channel.close();

    }
}
