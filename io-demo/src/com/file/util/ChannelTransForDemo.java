package com.file.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/14 23:22
 * @desc desc
 * ========================================================
 */
public class ChannelTransForDemo {

    private final static Integer B_SIZE = 1024;

    public static void main(String[] args) throws IOException {
        FileChannel fc = new FileInputStream("完美主义.mp3").getChannel();
        FileChannel channel = new FileOutputStream("C:\\Users\\Administrator\\Desktop\\a.map3").getChannel();
       fc.transferTo(0,fc.size(),channel);
       fc.close();
       channel.close();
    }
}