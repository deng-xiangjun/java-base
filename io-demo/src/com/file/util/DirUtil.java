package com.file.util;

import javax.naming.spi.DirStateFactory;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/11 15:19
 * @desc ========================================================
 */
public class DirUtil {
    public static File[] local(File dir,String regex) {
       return dir.listFiles(new FilenameFilter() {
            private Pattern pattern = Pattern.compile(regex);
            @Override
            public boolean accept(File dir, String name) {
                return pattern.matcher(new File(name).getName()).matches();
            }
        });
    }
    public static File[] local(String path,String regex) {
       return local(new File(path),regex);
    }
    public static class TreeInfo {
        public  List<File> dirs = new ArrayList<>();
        public  List<File> files = new ArrayList<>();

        public  void addAll(TreeInfo treeInfo) {
            dirs.addAll(treeInfo.dirs);
            files.addAll(treeInfo.files);
        }
        static TreeInfo recurse(File start,String regex) {
            TreeInfo treeInfo = new TreeInfo();
            if(start.isDirectory()) {
                treeInfo.dirs.add(start);
                File[] files = start.listFiles();
                for (File file : files) {
                    if(file.isDirectory()) {
                        treeInfo.dirs.add(file);
                        treeInfo.addAll(recurse(file,regex));
                    }else {
                        if(file.getName().matches(regex)) {
                            treeInfo.files.add(file);
                        }
                    }
                }

            }else {
                treeInfo.files.add(start);
            }
           return treeInfo;
        }
    }
    public static TreeInfo walk(File start,String regex) {
        return TreeInfo.recurse(start,regex);
    }
    public static TreeInfo walk(String start,String regex) {
        return TreeInfo.recurse(new File(start),regex);
    }
    public static TreeInfo walk(File start) {
        return TreeInfo.recurse(start,".*");
    }
    public static TreeInfo walk(String start) {
        return TreeInfo.recurse(new File(start),".*");
    }
    public static String pFormat(Collection<?> c) {
        if(c.size()==0) {
           return "[]";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (Object elem : c) {
            if(c.size()!=1) {
                sb.append("\n  ");
            }
            sb.append(elem);
        }
        if(c.size()!=1) {
            sb.append("\n");
        }
        sb.append("]");
        return sb.toString();
    }

    public static void print(Object[] arr) {
        System.out.println(pFormat(Arrays.asList(arr)));
    }
    public static void print(Collection<?> c) {
        System.out.println(pFormat(c));
    }

    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        ArrayList<String> list1 = new ArrayList<>();
        list1.add("a");
        ArrayList<String> list2 = new ArrayList<>();
        list2.add("b");
        list2.add("c");
        System.out.println(pFormat(list));
        System.out.println(pFormat(list1));
        System.out.println(pFormat(list2));

    }
}
