package com.file.util;

import java.io.File;
import java.io.IOException;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/11 16:38
 * @desc ========================================================
 */
public class ProcessFiles1 {


    private String regex;
    private ProcessFiles.Strategy strategy;

    public ProcessFiles1(ProcessFiles.Strategy strategy, String regex) {
        this.strategy = strategy;
        this.regex = regex;
    }

    public void start(String[] args) throws IOException {
        if (args.length == 0) {
            processTreeDirectory(new File("."));
        } else {
            for (String arg : args) {
                File file = new File(arg);
                if (file.isDirectory()) {
                    processTreeDirectory(file);
                } else {
                    if (arg.matches(regex)) {
                        strategy.process(new File(arg).getCanonicalFile());
                    }

                }
            }
        }
    }

    public void processTreeDirectory(File root) throws IOException {
        for (File file : DirUtil.walk(root, regex).files) {
            strategy.process(file.getCanonicalFile());
        }
    }
}
