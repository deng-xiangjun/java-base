package com.file;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/24 20:45
 * @desc desc
 * ========================================================
 */
public class UsingBufferDemo {
    public static void main(String[] args) {
        String s = "abcdefg";
        char[] charArray = s.toCharArray();
        CharBuffer wrap = CharBuffer.wrap(charArray);
        System.out.println(wrap);
        changeBuffer(wrap);
        wrap.rewind();
        System.out.println(wrap);
    }

    public static void changeBuffer(CharBuffer charBuffer) {
        while (charBuffer.hasRemaining()) {
            charBuffer.mark();
            char c1 = charBuffer.get();
            char c2 = ' ';
            if (charBuffer.limit() != charBuffer.position()) {
                c2 = charBuffer.get();

            }
            charBuffer.reset();
            charBuffer = charBuffer.put(c2);
            if (charBuffer.limit() != charBuffer.position()) {
               charBuffer.put(c1);
            }
            System.out.println("position: "+charBuffer.position());
        }


    }
}
