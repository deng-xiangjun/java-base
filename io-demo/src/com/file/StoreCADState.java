package com.file;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/27 22:55
 * @desc desc
 * ========================================================
 */
public class StoreCADState {
    public static void main(String[] args) throws IOException {
        List<Shape> shapes = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            shapes.add(Shape.randomFactory());
        }
        for (Shape shape : shapes) {
            shape.setColor(Shape.GREEN);
        }
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("CADState.out"));
        Circle.serializeStaticState(oos);
        Square.serializeStaticState(oos);
        Line.serializeStaticState(oos);
        oos.writeObject(shapes);
        oos.close();
        System.out.println(shapes);
        //output
        // [class com.file.CircleShape{xPos=58, yPos=55, dimension=93color=3}
        //, class com.file.SquareShape{xPos=61, yPos=61, dimension=29color=3}
        //, class com.file.LineShape{xPos=68, yPos=0, dimension=22color=3}
        //, class com.file.CircleShape{xPos=7, yPos=88, dimension=28color=3}
        //, class com.file.SquareShape{xPos=51, yPos=89, dimension=9color=3}
        //, class com.file.LineShape{xPos=78, yPos=98, dimension=61color=3}
        //, class com.file.CircleShape{xPos=20, yPos=58, dimension=16color=3}
        //, class com.file.SquareShape{xPos=40, yPos=11, dimension=22color=3}
        //, class com.file.LineShape{xPos=4, yPos=83, dimension=6color=3}
        //, class com.file.CircleShape{xPos=75, yPos=10, dimension=42color=3}

    }
}
abstract class Shape implements Serializable {
    public static final int RED=1,BLUE=2,GREEN=3;
    private int xPos,yPos,dimension;
    private static Random rand = new Random(47);
    private static int counter=0;
    public abstract void setColor(int color);
    public abstract int getColor();
    public Shape (int xVal,int yVal,int dim) {
        xPos=xVal;
        yPos=yVal;
        dimension=dim;
    }

    @Override
    public String toString() {
        return getClass()+ "Shape{" +
                "xPos=" + xPos +
                ", yPos=" + yPos +
                ", dimension=" + dimension +
                "color="+getColor()+'}'+"\n";
    }
    public static Shape randomFactory() {
        int xVal=rand.nextInt(100);
        int yVal=rand.nextInt(100);
        int dim=rand.nextInt(100);
        switch (counter++%3) {
            default:
            case 0:return new Circle(xVal,yVal,dim);
            case 1:return new Square(xVal,yVal,dim);
            case 2:return new Line(xVal,yVal,dim);
        }
    }

}

class Circle extends Shape {
    private static int color=RED;
    public Circle(int xVal, int yVal, int dim) {
        super(xVal, yVal, dim);
    }

    @Override
    public void setColor(int newColor) {
        color=newColor;
    }

    @Override
    public int getColor() {
        return color;
    }
    public static void serializeStaticState(ObjectOutputStream oos) throws IOException {
        oos.writeInt(color);
    }
    public static void deSerializeStaticState(ObjectInputStream ois) throws IOException {
        color=ois.readInt();
    }
}
class Square extends Shape {
    private static int color;
    public Square(int xVal, int yVal, int dim) {
        super(xVal, yVal, dim);
        color=RED;
    }

    @Override
    public void setColor(int newColor) {
        color=newColor;
    }

    @Override
    public int getColor() {
        return color;
    }
    public static void serializeStaticState(ObjectOutputStream oos) throws IOException {
        oos.writeInt(color);
    }
    public static void deSerializeStaticState(ObjectInputStream ois) throws IOException {
        color=ois.readInt();
    }
}

class Line extends Shape {
    private static int color=RED;
    @Override
    public void setColor(int newColor) {
        color=newColor;
    }

    @Override
    public int getColor() {
        return color;
    }

    public Line(int xVal, int yVal, int dim) {
        super(xVal, yVal, dim);
    }

    public static void serializeStaticState(ObjectOutputStream oos) throws IOException {
        oos.writeInt(color);
    }
    public static void deSerializeStaticState(ObjectInputStream ois) throws IOException {
       color=ois.readInt();
    }

}

