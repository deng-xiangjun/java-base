package com.io;

import java.nio.charset.Charset;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/20 23:02
 * @desc desc
 * ========================================================
 */
public class CharSetDemo {
    public static void main(String[] args) {
        SortedMap<String, Charset> charSetsMap = Charset.availableCharsets();
        Set<String> keysSet = charSetsMap.keySet();
        for (String key : keysSet) {
            System.out.print(key);
            Charset charset = charSetsMap.get(key);
            Set<String> aliases = charset.aliases();
            System.out.print(": ");
            for (String alias : aliases) {
                System.out.print(alias);
                System.out.print(", ");
            }
            System.out.println();
        }
    }
}
