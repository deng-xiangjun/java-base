package com.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/20 22:48
 * @desc desc
 * ========================================================
 */
public class GetChannel {
    private static final Integer B_SIZE=1024;
    public static void main(String[] args) throws IOException {
        FileChannel fc = new FileOutputStream("data2.txt").getChannel();
       // fc.write(ByteBuffer.wrap("好好学习,天天向上".getBytes(StandardCharsets.UTF_8)));
       // fc.write(ByteBuffer.wrap("好好学习,天天向上".getBytes("UTF-16BE")));
        ByteBuffer buff = ByteBuffer.allocate(B_SIZE);
        buff.asCharBuffer().put("好好学习,天天向上");
        fc.write(buff);
        fc.close();
        fc=new FileInputStream("data2.txt").getChannel();
        buff.clear();
        fc.read(buff);
        buff.flip();
        System.out.println(buff.asCharBuffer());
//        buffer.rewind();
//        String encoding = System.getProperty("file.encoding");
//        System.out.println("Decoded using "+encoding+": "+ Charset.forName(encoding).decode(buffer));

    }
}
