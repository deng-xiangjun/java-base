package com.io;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.util.BitSet;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/20 23:12
 * @desc desc
 * ========================================================
 */
public class CharSetDemo01 {
    private static final Integer B_SIZE=1024;
   static BitSet bitSet = new BitSet(127);
     static  {
        for (int i = 32; i <=127 ; i++) {
            //给当前索引对应得值设为true
                bitSet.set(i);
        }
    }
    public static void isPrintAble(CharBuffer charBuffer) {
         charBuffer.rewind();
         //获得charBuffer当前位置也就是字符不能输出的位置
         while (bitSet.get(charBuffer.get()));
         //限制charBuffer
         charBuffer.limit(charBuffer.position()-1);
         //把charBuffer的索引设为0
         charBuffer.rewind();
    }
    public static void main(String[] args) {
        ByteBuffer buff = ByteBuffer.allocate(B_SIZE);
        CharBuffer buffer = buff.asCharBuffer().put("abcdefg" + (char) 0x01 + "HI");
        buffer.flip();
        System.out.println(buffer);
        isPrintAble(buffer);
        System.out.println(buffer);
    }
}
