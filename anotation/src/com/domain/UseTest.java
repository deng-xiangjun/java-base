package com.domain;

import com.anotation.Anno1;
import com.anotation.Anno2;

@Anno2
public class UseTest {

    //没有使用Test注解
    public void show(){
        System.out.println("UseTest....show....");
    }

    //使用Test注解
    @Anno1("1")
    public void method(){
        System.out.println("UseTest....method....");
    }

    //没有使用Test注解
    @Anno1("2")
    public void function(){
        System.out.println("UseTest....function....");
    }
}
