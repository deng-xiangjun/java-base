package com.service.impl;

import com.service.Userservice;

/**
 * @author xiangjun.deng
 * email 17670934918dxj@sina.com
 * @date 2021/8/12 16:08
 * @desc
 */
public class UserServiceImpl extends Userservice {
    @Override
    public void say() {
        System.out.println("hello world");
    }
}
