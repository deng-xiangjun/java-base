package com.test;

import com.anotation.Anno1;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * @author xiangjun.deng
 * email 17670934918dxj@sina.com
 * @date 2021/8/9 20:46
 * @desc
 */
public class AnotaionTest02 {
    public static void main(String[] args) {
        try {
            Class<?> clazz = Class.forName("com.service.impl.UserServiceImpl");
            Object o = clazz.newInstance();
            Method method = clazz.getDeclaredMethod("say");
            method.invoke(o);
            boolean flag = clazz.isAnnotationPresent(Anno1.class);
            System.out.println(flag);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
