package com.test;

import com.anotation.Anno1;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

/**
 * @author xiangjun.deng
 * email 17670934918dxj@sina.com
 * @date 2021/8/9 20:32
 * @desc
 */
public class AnotaionTest01 {
    public static void main(String[] args) {
        try {
            Class<?> clazz = Class.forName("com.domain.UserTest1");
            Constructor<?> conn = clazz.getDeclaredConstructor();
            Object o = conn.newInstance();
            Method[] methods = clazz.getDeclaredMethods();
            for (Method method : methods) {
                if (method.isAnnotationPresent(Anno1.class))
                {
                    Anno1 annotation = method.getAnnotation(Anno1.class);
                    String value = annotation.value();
                    String name = annotation.name();
                    System.out.println("value: "+value);
                    System.out.println("name: "+name);

                    method.invoke(o);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
