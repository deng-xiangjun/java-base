package com.decorate;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/12 0:17
 * @desc desc
 * ========================================================
 */
public abstract class Basic {
    public abstract void pourInto(String type);
}
