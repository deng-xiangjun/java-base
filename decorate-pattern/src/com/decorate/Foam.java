package com.decorate;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/11 19:59
 * @desc desc
 * ========================================================
 */
public class Foam extends CoffeeDecorator{

    public Foam(Basic basicCoffee) {

        super(basicCoffee);

    }

    @Override
    public void pourInto(String type) {
        type+="Foam";
        basicCoffee.pourInto(type);
    }
}
