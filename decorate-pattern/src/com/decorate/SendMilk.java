package com.decorate;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/11 19:59
 * @desc desc
 * ========================================================
 */
public class SendMilk extends CoffeeDecorator{

    public SendMilk(Basic basicCoffee) {
        super(basicCoffee);
    }

    @Override
    public void pourInto(String type) {
        type+="milk";
        basicCoffee.pourInto(type);
    }
}
