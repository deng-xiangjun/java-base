package com.decorate;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/11 19:56
 * @desc desc
 * ========================================================
 */
public abstract class CoffeeDecorator extends Basic {
    protected Basic basicCoffee;

    public Basic getBasicCoffee() {
        return basicCoffee;
    }

    public void setBasicCoffee(Basic basicCoffee) {
        this.basicCoffee = basicCoffee;
    }

    public CoffeeDecorator(Basic basicCoffee) {
        this.basicCoffee = basicCoffee;
    }

}
