package com;

import com.decorate.BasicCoffee;
import com.decorate.CoffeeDecorator;
import com.decorate.Foam;
import com.decorate.SendMilk;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/12 0:29
 * @desc desc
 * ========================================================
 */
public class TestDecorator {
    public static void main(String[] args) {
        CoffeeDecorator coffeeDecorator = new SendMilk(new SendMilk(new Foam(new BasicCoffee())));
        coffeeDecorator.pourInto("咖啡");
    }
}
