package com.test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author xiangjun.deng
 * email 17670934918dxj@sina.com
 * @date 2021-9-28 10:15
 * @desc 测试subList
 */
public class Test01 {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();

        for (int i = 0; i < 1001; i++) {
            list.add(i+"");
        }
        List<String> resList = new ArrayList<>();
        for (int i = 0; i < list.size(); i=i+999) {
            if(list.size()>=999)
            {
                int toIndex = i+999;
                if(toIndex>list.size())
                {
                    List<String> subList = list.subList(i, list.size());
                    String s = subList.get(subList.size() - 1);
                    System.out.println(s);
                    System.out.println(subList.get(0));
                    System.out.println(subList.size());
                    resList.addAll(subList);
                }
                else {
                    List<String> subList = list.subList(i, toIndex);
                    System.out.println(subList.get(0));
                    System.out.println(subList.get(subList.size()-1));
                    System.out.println(subList.size());
                }


            }
            else{
                List<String> subList = list.subList(i, list.size());
                System.out.println(subList.size());
            }
        }
    }
}

