package com.dxj;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author xiangjun.deng
 * email 17670934918dxj@sina.com
 * @date 2021/7/26 14:52
 * @desc
 */
public class Test01 {
    public static void main(String[] args) {
        String dateStr = "2012-01-01";
        ParsePosition parsePosition = new ParsePosition(1);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date parse = sdf.parse(dateStr, parsePosition);
        System.out.println(parse);
        System.out.println(parsePosition.getIndex());
        System.out.println(dateStr.length());
    }
}
