package com.dxj;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author xiangjun.deng
 * email 17670934918dxj@sina.com
 * @date 2021/9/1 11:14
 * @desc 在date後面加上時分秒
 */
public class Test02 {
    public static void main(String[] args) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        String date = "2021/8/31";
        Date newDate = sdf.parse(date);
        Calendar calendar=Calendar.getInstance();
        calendar.setTime(newDate);
        calendar.add(Calendar.DAY_OF_MONTH, +1);//+1今天的时间加一天
        calendar.add(Calendar.HOUR,23);//时
        calendar.add(Calendar.MINUTE,59);//时
        calendar.add(Calendar.SECOND,59);//秒
        System.out.println(calendar.getTime());
    }
}
