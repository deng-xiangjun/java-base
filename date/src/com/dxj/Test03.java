package com.dxj;

/**
 * @author xiangjun.deng
 * email 17670934918dxj@sina.com
 * @date 2021/9/3 8:37
 * @desc long类型强转int类型精度丢失的问题
 */
public class Test03 {
    public static void main(String[] args) {
        long i = 604800;
        int res= (int) (i/60/60/24);
        System.out.println(res);
    }
}
