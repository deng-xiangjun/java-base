package com.dxj;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author xiangjun.deng
 * email 17670934918dxj@sina.com
 * @date 2021-9-30 16:26
 * @desc
 */
public class Test05 {
    public static void main(String[] args) throws ParseException {
        SimpleDateFormat f = new SimpleDateFormat("yyyy/M/d hh:mm:ss");
        SimpleDateFormat df = new SimpleDateFormat("yyyy/M/d 23:59:59");
        List<Date> dateList = new ArrayList<Date>();
        Date dateFm = new Date(1632844800000L);
        Date dateTo = new Date(1632844800000L);
            dateList.add(f.parse(df.format(dateFm)));
            // 两个日期之间的日期
            Calendar startCalendar = Calendar.getInstance();
            Calendar endCalendar = Calendar.getInstance();
            startCalendar.setTime(dateFm);
            endCalendar.setTime(dateTo);
            //endCalendar.add(Calendar.DAY_OF_MONTH, -1);// 小于不等于结束日期
            while (true) {
                startCalendar.add(Calendar.DAY_OF_MONTH, 1);
                Date time = startCalendar.getTime();
                String format = f.format(time);
                System.out.println(format);
                if (startCalendar.getTimeInMillis() <= endCalendar.getTimeInMillis()) {
                    dateList.add(f.parse(df.format(time)));
                } else {
                    break;
                }
            }

    }


}
