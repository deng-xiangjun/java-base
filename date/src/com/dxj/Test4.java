package com.dxj;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author xiangjun.deng
 * email 17670934918dxj@sina.com
 * @date 2021/9/5 18:16
 * @desc
 */
public class Test4 {
    public static void main(String[] args) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String str1="2021-08-21 18:17:20";
        String str2="2021-08-21 17:17:20";
        Date date1 = sdf.parse(str1);
        Date date2 = sdf.parse(str2);
        System.out.println(date1.getTime()-date2.getTime());
    }
}
