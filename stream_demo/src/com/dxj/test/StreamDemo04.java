package com.dxj.test;

import com.dxj.pojo.Person;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author xiangjun.deng
 * email 17670934918dxj@sina.com
 * @date 2021-12-7 15:00
 * @desc
 */
public class StreamDemo04 {
    public static void main(String[] args) {
        List<Person> list = new ArrayList<>();
        Person person01 = new Person();
        person01.setAge(120);
        person01.setName("zhansan");

        Person person02 = new Person();
        person02.setAge(19);
        person02.setName("zhansan");
        Person person03 = new Person();
        person03.setAge(120);
        person03.setName("zhansan");

        list.add(person01);
        list.add(person02);
        list.add(person03);

        Set<Integer> collect = list.stream().map(Person::getAge).collect(Collectors.toSet());
        System.out.println(collect);

    }
}
