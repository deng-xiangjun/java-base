package com.dxj.test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author xiangjun.deng
 * email 17670934918dxj@sina.com
 * @date 2021-12-7 14:07
 * @desc
 */
public class MyStream1 {
    public static void main(String[] args) {
        //集合的批量添加
        ArrayList<String> list1 = new ArrayList<>();
        list1.add("张三丰");
        list1.add("张无忌");
        list1.add("张翠山");
        list1.add("王二麻子");
        list1.add("张良");
        list1.add("谢广坤");

        //遍历list1把以张开头的元素添加到list2中。
        ArrayList<String> list2 = new ArrayList<>();
        for (String s : list1) {
            if (s.startsWith("张")) {
                list2.add(s);
            }
        }
        //遍历list2集合，把其中长度为3的元素，再添加到list3中。
        ArrayList<String> list3 = new ArrayList<>();
        for (String s : list2) {
            if (s.length() == 3) {
                list3.add(s);
            }
        }
        for (String s : list3) {
            System.out.println(s);
        }
    }
}

