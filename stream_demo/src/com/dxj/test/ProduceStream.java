package com.dxj.test;

import java.util.*;
import java.util.stream.Stream;

/**
 * @author xiangjun.deng
 * email 17670934918dxj@sina.com
 * @date 2021-12-7 14:14
 * @desc
 */
public class ProduceStream {
//    Collection体系集合使用默认方法stream()生成流， default Stream<E> stream()
//
//  Map体系集合把Map转成Set集合，间接的生成流
//
//   数组通过Arrays中的静态方法stream生成流
//
//  同种数据类型的多个数据通过==Stream接口的静态方法of(T... values)生成流==
    public static void main(String[] args) {
        //Collection体系的集合可以使用默认方法stream()生成流
        List<String> list = new ArrayList<String>();
        Stream<String> listStream = list.stream();

        Set<String> set = new HashSet<String>();
        Stream<String> setStream = set.stream();

        //Map体系的集合间接的生成流
        Map<String,Integer> map = new HashMap<String, Integer>();
        Stream<String> keyStream = map.keySet().stream();
        Stream<Integer> valueStream = map.values().stream();
        Stream<Map.Entry<String, Integer>> entryStream = map.entrySet().stream();

        //数组可以通过Arrays中的静态方法stream生成流
        String[] strArray = {"hello","world","java"};
        Stream<String> strArrayStream = Arrays.stream(strArray);

        //同种数据类型的多个数据可以通过Stream接口的静态方法of(T... values)生成流
        Stream<String> strArrayStream2 = Stream.of("hello", "world", "java");
        Stream<Integer> intStream = Stream.of(10, 20, 30);
    }
}
