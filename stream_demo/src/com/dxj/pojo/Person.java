package com.dxj.pojo;

/**
 * @author xiangjun.deng
 * email 17670934918dxj@sina.com
 * @date 2021-12-7 14:59
 * @desc
 */
public class Person {
    private String name;
    private Integer age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
