import java.util.ArrayList;
import java.util.List;

/**
 * @author xiangjun.deng
 * email 17670934918dxj@sina.com
 * @date 2021/8/3 11:26
 * @desc
 */
public class stringjoin {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("asx");
        list.add("asd");
        list.add("abc");
        String join = String.join(",", list);
        System.out.println(join);
    }
}
