/**
 * @author xiangjun.deng
 * email 17670934918dxj@sina.com
 * @date 2021/9/8 10:24
 * @desc
 */
public class StringFormat {
    public static void main(String[] args) {
        String dxl = String.format("code.message: %s, info: %s", "001", "空指针异常");
        System.out.println(dxl);
    }
}
