import java.util.Arrays;

/**
 * @author xiangjun.deng
 * email 17670934918dxj@sina.com
 * @date 2021-9-28 13:08
 * @desc
 */
public class StringSplit {
    public static void main(String[] args) {
        try {
            String[] split = "526050104586".split("-");
            String s = split[0];
            String s1 = split[1];
            System.out.println(split.length);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println(formatStr(e));
        }
    }
    public static String formatStr(Exception e)
    {
        String format = String.format("代码位置: %s", Arrays.asList(e.getStackTrace()).toString() + "," + e.toString());
        return format;
    }
}
