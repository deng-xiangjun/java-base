package com;

import com.domain.Apple;
import com.domain.Person;
import com.domain.Student;

/**
 * @author xiangjun.deng
 * email 17670934918dxj@sina.com
 * @date 2021/7/26 13:26
 * @desc
 */
public class App {
    public static void main(String[] args) throws ClassNotFoundException {
//        System.out.println(Class.forName("com.App").getPackage().getName());
//        Person.sayHello();

        Apple apple = new Apple();
        apple.setSize("5cm");
        apple.processApple(new Person());

        Student s = new Student();
        System.out.println(s.getSs());
        System.out.println(s.getS());

    }
}
