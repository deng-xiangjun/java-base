package com.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * @author xiangjun.deng
 * email 17670934918dxj@sina.com
 * @date 2021/7/26 13:23
 */
public class Person {
    private String name;
    private Integer age;

    public Person(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    public Person() {
        List<Integer> list = new ArrayList<>();
        System.out.println("constructor 运行了");
        sayHi();

    }

    public static void sayHello()
    {
        System.out.println("hello world");
    }
    public static void sayHi()
    {
        System.out.println("hi...");
    }

    public void precess(Apple apple) {
        System.out.println(apple.getSize());
    }



}
