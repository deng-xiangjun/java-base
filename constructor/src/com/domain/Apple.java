package com.domain;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021-12-10 16:22
 * @desc TODO
 * ========================================================
 */
public class Apple {
    private String size;

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public void processApple(Person person) {
        person.precess(this);
    }
}
