package com;

/**
 * @author xiangjun.deng
 * email 17670934918dxj@sina.com
 * @date 2021-10-19 11:07
 * @desc
 */
public class ConstructorTest01 {
    public static void main(String[] args) {
        Tester2 t = new Tester2();
        System.out.println("t.s1: " + t.s1);
        System.out.println("t.s2: " + t.s2);
        System.out.println("t.s3: " + t.s3);
    }


}
class Tester01{
    String s;
}

class Tester2 {
    String s1;
    String s2 = "hello";
    String s3;
    Tester2() { s3 = "good-bye"; }
}
