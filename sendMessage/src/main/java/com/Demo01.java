package com;

import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;

/**
 * @author xiangjun.deng
 * email 17670934918dxj@sina.com
 * @date 2021/8/17 14:22
 * @desc
 */
public class Demo01 {
    public static final String account = "17670934918dxj@sina.com";
    public static final  String password="9b529e95c44f613e";
    public static final  String myEmailSMTPHost="smtp.sina.com";
    public static String receiveMailAccount = "389415852@qq.com";
    public static void main(String[] args) throws Exception {
//        邮件创建步骤:
//        1. 创建一个邮件对象（MimeMessage）；
//        2. 设置发件人，收件人，可选增加多个收件人，抄送人，密送人；
//        3. 设置邮件的主题（标题）；
//        4. 设置邮件的正文（内容）；
//        5. 设置显示的发送时间；
//        6. 保存到本地。
        // 1. 创建一封邮件
        Properties props = new Properties();                // 用于连接邮件服务器的参数配置（发送邮件时才需要用到）
        props.setProperty("mail.transport.protocol", "smtp");   // 使用的协议（JavaMail规范要求）
        props.setProperty("mail.smtp.host", myEmailSMTPHost);   // 发件人的邮箱的 SMTP 服务器地址
        props.setProperty("mail.smtp.auth", "true");

        Session session= Session.getInstance(props);        // 根据参数配置，创建会话对象（为了发送邮件准备的）
        session.setDebug(true);
        MimeMessage message = new MimeMessage(session);     // 创建邮件对象
        // 2. From: 发件人
        //    其中 InternetAddress 的三个参数分别为: 邮箱, 显示的昵称(只用于显示, 没有特别的要求), 昵称的字符集编码
        //    真正要发送时, 邮箱必须是真实有效的邮箱。
        message.setFrom(new InternetAddress(account, "大魔王", "UTF-8"));

        // 3. To: 收件人
        message.setRecipient(MimeMessage.RecipientType.TO, new InternetAddress(receiveMailAccount, "USER_CC", "UTF-8"));

        message.setSubject("使用javaMail发送邮件", "UTF-8");

        // 5. Content: 邮件正文（可以使用html标签）
        message.setContent("hello world", "text/html;charset=UTF-8");

        message.setSentDate(new Date());

        // 7. 保存前面的设置
        message.saveChanges();

        // 8. 根据 Session 获取邮件传输对象
        Transport transport = session.getTransport();


        transport.connect(account, password);

        transport.sendMessage(message, message.getAllRecipients());

        // 7. 关闭连接
        transport.close();

        // 8. 将该邮件保存到本地
        OutputStream out = new FileOutputStream("myEmail.eml");
        message.writeTo(out);
        out.flush();
        out.close();
    }
}
