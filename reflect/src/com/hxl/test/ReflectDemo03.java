package com.hxl.test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

/**
 * @author xiangjun.deng
 * email 17670934918dxj@sina.com
 * @date 2021/8/9 13:51
 * @desc 反射获取成员变量
 */
public class ReflectDemo03 {
    public static void main(String[] args) {
        try {
            Class<?> clzz = Class.forName("com.hxl.domain.Teacher");
            //method1(clzz);
            method2(clzz);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取所有的Field对象
     * @param clzz
     */
    public static void method1(Class clzz) {
        Field[] fields = clzz.getDeclaredFields();
        for (Field field : fields) {
            System.out.println(field.getName());
        }
    }

    /**
     * 获取制定对象的属性值,给指定的对象赋值
     * @param clzz
     */
    public static void method2(Class clzz) {
        try {
            Constructor conn = clzz.getDeclaredConstructor();
            conn.setAccessible(true);
            Object o = conn.newInstance();
            Field[] fields = clzz.getDeclaredFields();
            for (Field field : fields) {
                if("money".equals(field.getName()))
                {
                    field.setAccessible(true);
                    Object o1 = field.get(o);
                    System.out.println(o1);
                }
                if("name".equals(field.getName()))
                {
                    field.set(o,"张三");
                    System.out.println(o);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

}
