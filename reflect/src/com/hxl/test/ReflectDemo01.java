package com.hxl.test;

import com.hxl.domain.Student;

/**
 * @author xiangjun.deng
 * email 17670934918dxj@sina.com
 * @date 2021/8/9 11:49
 * @desc
 */
public class ReflectDemo01 {
    /*获取Class对象的三种方法
      1.类名.class属性

      2.对象名.getClass()方法

       3.Class.forName(全类名)方法*/
    public static void main(String[] args) throws ClassNotFoundException {
        Class<?> clzz = Class.forName("com.hxl.domain.Student");
        System.out.println(clzz);

        Class<Student> clzz1 = Student.class;
        System.out.println(clzz1);

        Student student = new Student();
        Class<? extends Student> clzz2 = student.getClass();
        System.out.println(clzz2);
    }
}
