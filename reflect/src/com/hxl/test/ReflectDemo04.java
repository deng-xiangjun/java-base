package com.hxl.test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static com.hxl.test.ReflectDemo03.method1;

/**
 * @author xiangjun.deng
 * email 17670934918dxj@sina.com
 * @date 2021/8/9 14:12
 * @desc 反射获取方法对象
 */
public class ReflectDemo04 {
    public static void main(String[] args) {
        try {
            Class<?> clzz = Class.forName("com.hxl.domain.Student");
            //method1(clzz);
            method5(clzz);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //获取所有的method对象
    private static void method1(Class<?> clzz) {
        Method[] methods = clzz.getDeclaredMethods();
        for (Method method : methods) {
            System.out.println(method);
        }
    }
    private static void method2(Class clzz) throws ClassNotFoundException, NoSuchMethodException {
        //        Method getMethod(String name, Class<?>... parameterTypes) ：
//                                返回单个公共成员方法对象
        //2.获取成员方法function1
        Method method1 = clzz.getMethod("function1");
        //3.打印一下
        System.out.println(method1);
    }

    private static void method3(Class clzz) throws ClassNotFoundException, NoSuchMethodException {
        //        Method getMethod(String name, Class<?>... parameterTypes) ：
//                                返回单个公共成员方法对象
        //2.获取成员方法function1
        Method method1 = clzz.getMethod("function1",String.class);
        //3.打印一下
        System.out.println(method1);
    }
    //获取私有变量
    private static void method4(Class clzz) throws ClassNotFoundException, NoSuchMethodException {
        //        Method getMethod(String name, Class<?>... parameterTypes) ：
//                                返回单个公共成员方法对象
        //2.获取成员方法function1
        Method method1 = clzz.getDeclaredMethod("show");
        //3.打印一下
        System.out.println(method1);
    }

    private static void method5(Class clzz) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException {
        //        Method getMethod(String name, Class<?>... parameterTypes) ：
//                                返回单个公共成员方法对象
        //2.获取成员方法function1
        Object o = clzz.newInstance();
        Method method1 = clzz.getDeclaredMethod("function1",String.class);
        method1.invoke(o,"zhangsan");
        //3.打印一下
        System.out.println(method1);
    }
}
