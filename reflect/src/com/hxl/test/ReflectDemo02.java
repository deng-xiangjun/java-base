package com.hxl.test;

import java.lang.reflect.Constructor;

/**
 * @author xiangjun.deng
 * email 17670934918dxj@sina.com
 * @date 2021/8/9 11:51
 * @desc
 */
public class ReflectDemo02 {
    //获取构造方法
    public static void main(String[] args) {
        try {
            Class<?> clazz = Class.forName("com.hxl.domain.Student");
            Constructor<?> constructor = clazz.getDeclaredConstructor(String.class);
            //被private修饰的成员,不能直接使用的
            //如果用反射强行获取并使用,需要临时取消访问检查
            constructor.setAccessible(true);
            Object o = constructor.newInstance("张三");
            System.out.println(o);
            System.out.println(constructor);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
