import java.util.Locale;
import java.util.ResourceBundle;

/**
 * @author xiangjun.deng
 * email 17670934918dxj@sina.com
 * @date 2021/7/30 16:19
 * @desc
 */
public class App {
    public static void main(String[] args) {
        ResourceBundle bundle = ResourceBundle.getBundle("my",new Locale("en","US"));
        String cancel = bundle.getString("cancelKey");
        System.out.println(cancel);

        bundle = ResourceBundle.getBundle("my", Locale.US);
        cancel = bundle.getString("cancelKey");
        System.out.println(cancel);

        bundle = ResourceBundle.getBundle("my", Locale.getDefault());
        cancel = bundle.getString("cancelKey");
        System.out.println(cancel);

        bundle = ResourceBundle.getBundle("my", Locale.GERMAN);
        cancel = bundle.getString("cancelKey");
        System.out.println(cancel);

        bundle = ResourceBundle.getBundle("my");
        for (String key : bundle.keySet()) {
            System.out.println(bundle.getString(key));
        }

    }
}
