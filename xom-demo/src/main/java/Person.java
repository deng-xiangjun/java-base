import nu.xom.*;

import java.io.*;
import java.sql.Array;
import java.time.Period;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/29 0:30
 * @desc desc
 * ========================================================
 */
public class Person extends ArrayList<Person> {
    private String firstName;
    private String lastName;
    public Person(String lastName,String firstName) {
        this.firstName=firstName;
        this.lastName=lastName;
    }
    public Element getXml() {
        Element  person = new Element("person");
        Element first = new Element("firstName");
        first.appendChild(firstName);
        Element last = new Element("lastName");
        last.appendChild(lastName);
        person.appendChild(first);
        person.appendChild(last);
        return person;
    }
    public Person(Element element) {
         firstName = element.getFirstChildElement("firstName").getValue();
         lastName = element.getFirstChildElement("lastName").getValue();
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }

    public static void format(OutputStream os, Document doc) throws IOException {
        Serializer serializer = new Serializer(os, "UTF-8");
        serializer.setIndent(4);
        serializer.setMaxLength(64);
        serializer.write(doc);
        serializer.flush();
    }
    public Person(String firstName) throws ParsingException, IOException {
        Document document = new Builder().build("people.xml");
        Element rootElement = document.getRootElement();
        System.out.println("rootElement"+rootElement);
        Elements childElements = rootElement.getChildElements();
        System.out.println("childElements"+childElements);
        for (Element childElement : childElements) {
            add(new Person(childElement));
        }
    }
    public static void main(String[] args) throws IOException {
        List<Person> personList = Arrays.asList(new Person("Dr. Bunsen", "Honeydew"),
                new Person("思涵", "成吉"),
                new Person("世民", "李"));
        Element element = new Element("People");
        for (Person person : personList) {
            element.appendChild(person.getXml());
        }
        File file = new File("people.xml");
        OutputStream os = new BufferedOutputStream(new FileOutputStream(file));
        Document doc = new Document(element);
        Person.format(System.out,doc);
        Person.format(os,doc);
    }
}
