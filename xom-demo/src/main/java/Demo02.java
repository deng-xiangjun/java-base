import nu.xom.ParsingException;

import java.io.IOException;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021/12/29 0:51
 * @desc desc
 * ========================================================
 */
public class Demo02 {
    public static void main(String[] args) throws ParsingException, IOException {
        Person person = new Person("people.xml");
        for (Person person1 : person) {
            System.out.println(person1);
        }
    }
}
