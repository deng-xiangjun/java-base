

import nu.xom.Document;
import nu.xom.Element;
import nu.xom.Serializer;

import java.io.IOException;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021-12-28 16:55
 * @desc desc
 * ========================================================
 */
public class Demo01 {
    public static void main(String[] args) {
        Element root = new Element("root");
        for (int i = 0; i < 10; i++) {
            Element child = new Element("child");
            child.appendChild("Hello world!"+i+"");
            root.appendChild(child);
        }
        Document doc = new Document(root);
        try {
            Serializer serializer = new Serializer(System.out, "ISO-8859-1");
            serializer.setIndent(4);
            serializer.setMaxLength(64);
            serializer.write(doc);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //System.out.println(doc.toXML());
    }
}
