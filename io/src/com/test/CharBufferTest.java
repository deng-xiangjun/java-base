package com.test;

import java.io.FileOutputStream;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021-12-16 9:17
 * @desc desc
 * ========================================================
 */
public class CharBufferTest {
    public static void main(String[] args) {
        CharBuffer allocate = CharBuffer.allocate(50);
        allocate.put("好好学习，天天向上");
        allocate.flip();
        int i=0;
        while (allocate.hasRemaining()) {
            System.out.println(allocate.charAt(i));
            i++;
        }
    }
}
