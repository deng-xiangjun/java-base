package com.io;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021-12-15 17:15
 * @desc desc
 * ========================================================
 */
public class BufferToTest {
    private final static Integer B_SIZE=1024;
    public static void main(String[] args) throws IOException {
        //返回虚拟机默认字符集
        System.out.println(Charset.defaultCharset());
        //展示系统默认的字符集
        String encoding = System.getProperty("file.encoding");
        System.out.println(encoding);
        FileChannel fs = new FileOutputStream("data2.txt").getChannel();
        fs.write(ByteBuffer.wrap("some text".getBytes("UTF-16BE")));
        fs.close();
        FileChannel in = new FileInputStream("data2.txt").getChannel();
        ByteBuffer buffer = ByteBuffer.allocate(B_SIZE);
        in.read(buffer);
        buffer.flip();
        //乱码，
        System.out.println(buffer.asCharBuffer());//獯浥⁴數
        //使用平台默认的字符集进行解码 some text
        System.out.println(encoding);
        Charset charset = Charset.forName(encoding);
        System.out.println(charset.decode(buffer));
        in.close();
        //USEING charBuffer to write through;
         fs = new FileOutputStream("data2.txt").getChannel();
        ByteBuffer buff = ByteBuffer.allocate(24);
        //buff.asCharBuffer().put("好好学习");
        fs.write(ByteBuffer.wrap("好好学习".getBytes()));
        System.out.println(buff.position());
        System.out.println(buff.position());
        fs.close();
        in = new FileInputStream("data2.txt").getChannel();
        buff.clear();
        in.read(buff);
        System.out.println(buff.position());
        buff.flip();

        //System.out.println(buff.asCharBuffer());
        System.out.println(charset.decode(buff));
        in.close();
    }
}
