package com.io;

import java.io.*;
import java.util.*;
import java.util.stream.Stream;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021-12-14 8:46
 * @desc
 * ========================================================
 */
public class TextFile extends ArrayList<String> {
    public static String read(String fileName)  {
        StringBuilder sb = new StringBuilder();
        BufferedReader  br=null;
        try {
            br = new BufferedReader(new FileReader(new File(fileName).getAbsoluteFile()));
            String line=null;
            while((line = br.readLine())!=null) {
                sb.append(line);
                sb.append("\n");
            }
        }
        catch (IOException e) {
           throw new  RuntimeException(e);
        }finally {
            if(br !=null) {
                try {
                    br.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        return sb.toString();
    }

    public static void write(String filename,String text) {
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(new BufferedWriter(new FileWriter(new File(filename).getAbsoluteFile())));
            pw.print(text);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (pw!=null) {
                pw.close();
            }
        }
    }

    public TextFile(String filename,String regex) {
        super(Arrays.asList(read(filename).split(regex)));
    }

    public TextFile(String fileName) {
        this(fileName,"\n");
    }

    public  void write(String fileName) {
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(fileName);
            for (String item : this) {
                pw.println(item);
            }

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } finally {
           if(pw != null) {
               pw.close();
           }
        }
    }

    public static void main(String[] args) {
       // TextFile textFile = new TextFile("b.txt","\\.");
       // textFile.write("e.txt");
//        String text = TextFile.read("a.txt");
//        TextFile.write("d.txt",text);

        String text = TextFile.read("e.txt");
        char[] chars = text.toCharArray();
        Map<Character,Integer> map = new HashMap<>();
        for (int i = 0; i < chars.length; i++) {
            if(map.containsKey(chars[i])) {
                Integer count = map.get(chars[i]);
                count++;
                map.put(chars[i],count);
            }else {
                map.put(chars[i],1);
            }
        }

        Set<Character> keysSet = map.keySet();
        for (Character character : keysSet) {
            Character key = character;
            Integer count = map.get(key);
            System.out.println("key="+key+" value="+count);
        }
    }
}





