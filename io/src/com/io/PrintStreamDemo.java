package com.io;

import java.io.PrintWriter;
import java.io.Writer;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021-12-14 13:37
 * @desc TODO
 * ========================================================
 */
public class PrintStreamDemo {
    public static void main(String[] args) {
        PrintWriter pw = new PrintWriter(System.out);
        pw.write("好好学习，天天向上");
        pw.close();
    }
}
