package com.io;

import java.io.PrintWriter;
import java.nio.ByteBuffer;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021-12-16 17:31
 * @desc 往缓存区放入基本数据，
 * ========================================================
 */
public class ByteBufferDemo {
    private final static Integer B_SIZE=1024;

    public static void main(String[] args) {
        ByteBuffer buffer = ByteBuffer.allocate(B_SIZE);
        int i = 0;
        while (i++<buffer.limit()) {
            if(buffer.get()!=0) {
                System.out.println("nonzero");
            }
            System.out.println("i = "+i);
        }
        buffer.rewind();
        buffer.asCharBuffer().put("Howdy!");
        char c;
        while ((c=buffer.getChar())!=0) {
            System.out.print(c+" ");
        }
        buffer.rewind();
        buffer.asShortBuffer().put((short)471142);
        System.out.println(buffer.getShort());
        buffer.asIntBuffer().put(99471142);
        System.out.println(buffer.getInt());
        buffer.rewind();
        buffer.asLongBuffer().put(99471142);
        System.out.println(buffer.getLong());
        buffer.asFloatBuffer().put(99471142);
        System.out.println(buffer.getFloat());
        buffer.asDoubleBuffer().put(99471142);
        System.out.println(buffer.getDouble());
    }
}
