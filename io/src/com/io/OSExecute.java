package com.io;

import com.OSExecuteException;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021-12-14 14:26
 * @desc TODO
 * ========================================================
 */
public class OSExecute {
    public static void command(String command) {
        boolean err = false;
        try {
            Process ps = new ProcessBuilder(command.split(" ")).start();
            BufferedReader results = new BufferedReader(new InputStreamReader(ps.getInputStream()));
            String s;
            while ((s=results.readLine())!=null && s.length()!=0) {
                System.out.println(s);
            }
            BufferedReader errs = new BufferedReader(new InputStreamReader(ps.getErrorStream(),"gbk"));
            while ((s=errs.readLine())!=null) {
                System.err.println("errs = " + s);
                err=true;
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        if(err) {
            throw new OSExecuteException("Errors executing "+command);
        }
    }
}
