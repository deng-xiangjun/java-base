package com.io;

import java.nio.charset.Charset;
import java.util.Set;
import java.util.SortedMap;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021-12-15 18:04
 * @desc 返回所有的字符集
 * ========================================================
 */
public class CharsetDemo {
    public static void main(String[] args) {
        SortedMap<String, Charset> map = Charset.availableCharsets();
        Set<String> set = map.keySet();
        for (String key : set) {
            StringBuilder sb = new StringBuilder(key);
            sb.append(":");
            Charset charset = map.get(key);
            Set<String> aliases = charset.aliases();
            for (String alias : aliases) {
                sb.append(alias+" ,");
            }
            System.out.println(sb.toString());
        }
    }
}
