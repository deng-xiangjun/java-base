package com.io;

import java.util.BitSet;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021-12-16 11:15
 * @desc desc
 * ========================================================
 */
public class BitSetDemo {
    public static void main(String[] args) {
        BitSet bitSet1 = new BitSet(16);
        BitSet bitSet2 = new BitSet(16);

        for (int i = 0; i < 16; i++) {
            if(i%2==0) {
                bitSet1.set(i);
            }
            if(i%5==0){
                bitSet2.set(i);
            }
        }
        System.out.println(bitSet1.get(2));
        System.out.println(bitSet1.get(1));
        System.out.println(bitSet1.get(10));
        System.out.println(bitSet1);
        System.out.println(bitSet2);

    }
}
