package com.io;

import java.io.*;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021-12-14 13:49
 * @desc TODO
 * ========================================================
 */
public class RedirectIODemo {
    public static void main(String[] args) throws IOException {
        PrintStream console = System.out;
        //创建一个输入缓冲流
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream("a.txt"));
        //创建一个输出流
        PrintStream pos = new PrintStream(new FileOutputStream("f.txt"),true);
        //重定向流
        System.setIn(bis);
        //System.setOut(pos);
        System.setErr(pos);
        //bis.close();
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String line;
        while((line = br.readLine())!=null && line.length()!=0) {
            System.out.println(line);
        }
        //再把系统输出流给重定向回来
        System.setOut(console);
        pos.close();

    }
}
