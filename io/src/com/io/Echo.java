package com.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Locale;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021-12-14 11:59
 * @desc TODO
 * ========================================================
 */
public class Echo {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String line;
        while ((line = br.readLine())!=null && line.length() !=0) {
            System.out.println(line.toUpperCase());
        }


    }
}
