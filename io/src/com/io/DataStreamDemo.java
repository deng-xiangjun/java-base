package com.io;

import java.io.*;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021-12-12 16:50
 * @desc TODO
 * ========================================================
 */
public class DataStreamDemo {
    public static void main(String[] args) throws IOException {

        DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("C:\\Users\\Administrator\\Desktop\\data.txt")));
        dos.writeDouble(3.14159);
        //dos.writeUTF("要努力，要自律");
        dos.writeDouble(1.41413);
        //dos.writeUTF("Square root of 2");
        dos.close();

        DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream("C:\\Users\\Administrator\\Desktop\\data.txt")));
        System.out.println(dis.readDouble());
        //System.out.println(dis.readUTF());
        System.out.println(dis.readDouble());
        //System.out.println(dis.readUTF());
    }
}
