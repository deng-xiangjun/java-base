package com.io;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * =======================================================
 *
 * @author dxj
 * @date 2021-12-14 11:11
 * @desc TODO
 * ========================================================
 */
public class BinaryFile {
    public static byte[] read(File bFile) {
        BufferedInputStream in = null;
        try {
            in = new BufferedInputStream(new FileInputStream(bFile));
            byte[] byteArr = new byte[in.available()];
            in.read(byteArr);
            return byteArr;
        } catch (IOException e) {
           throw new RuntimeException(e);
        } finally {
            if(in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static byte[] read(String fileName) {
       return read(new File(fileName).getAbsoluteFile());
    }
    private static String file = "D:\\testPros\\java-base\\io\\out\\production\\io\\com\\io\\DataStreamDemo.class";
    public static void main1(String[] args) {
        byte[] bytes = read(file);
        System.out.println(bytes.length);
        Map<Byte,Integer> map = new HashMap<>();

        for (int i = 0; i < bytes.length; i++) {
            if(map.containsKey(bytes[i])) {
                Integer count = map.get(bytes[i]);
                count++;
                map.put(bytes[i],count);
            }else {
                map.put(bytes[i],1);
            }
        }

        Set<Byte> keysSet = map.keySet();
        for (Byte byteKey : keysSet) {
            Byte key = byteKey;
            Integer count = map.get(key);
            System.out.println("key="+key+" value="+count);
        }
    }
    final static byte[] signature =
            {(byte)202, (byte)254, (byte)186, (byte)190};
    public static void main(String[] args) {
        DirUtil.TreeInfo treeInfo = DirUtil.walk(".", ".*\\.class");
        List<File> files = treeInfo.files;
        for (File file1 : files) {
            System.out.println(file1.getParent());
            System.out.println(file1.getAbsoluteFile());
            byte[] bytes = read(file1);
            for (int i = 0; i < bytes.length; i++) {
                System.out.println(bytes[i]);
            }
            for (int i = 0; i < signature.length; i++) {
                if(bytes[i]!=signature[i]) {
                    System.err.println(file + " is corrupt!");
                    break;
                }
            }
        }
    }
}
